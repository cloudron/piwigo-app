#!/bin/bash

set -eu

echo "=> Ensure directories and permissions"
mkdir -p /run/piwigo /app/data/_data /app/data/galleries /app/data/upload /app/data/plugins /app/data/local/config /app/data/language /app/data/themes

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# setup waits for apache to start and configures piwigo
setup() {
    if [[ ! -f /app/data/_data/dummy.txt ]]; then
        echo "=> Detected first run"
        cp -r /app/code/_plugins/* /app/data/plugins/
        cp -r /app/code/_themes/* /app/data/themes/
        cp -r /app/code/_language/* /app/data/language/
        cp -r /app/code/_local/* /app/data/local/
        cp /app/code/_data_old/dummy.txt /app/data/_data/
    fi

    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "=> Waiting for apache2 to start"
        sleep 3
    done

    echo "=> Fixup permissions"
    chown -R www-data.www-data /app/data /run/piwigo

    echo "=> Setup piwigo"

    curl -L -X POST --data "?language=en_UK&install=true&dbhost=${CLOUDRON_MYSQL_HOST}&dbuser=${CLOUDRON_MYSQL_USERNAME}&dbpasswd=${CLOUDRON_MYSQL_PASSWORD}&dbname=${CLOUDRON_MYSQL_DATABASE}&admin_name=admin&admin_pass1=changeme&admin_pass2=changeme&admin_mail=admin@cloudron.local" "http://localhost:8000/install.php"

    sed -e "s/\$conf\['db_base'\] = .*/\$conf\['db_base'\] = getenv('CLOUDRON_MYSQL_DATABASE');/" \
        -e "s/\$conf\['db_user'\] = .*/\$conf\['db_user'\] = getenv('CLOUDRON_MYSQL_USERNAME');/" \
        -e "s/\$conf\['db_password'\] = .*/\$conf\['db_password'\] = getenv('CLOUDRON_MYSQL_PASSWORD');/" \
        -e "s/\$conf\['db_host'\] = .*/\$conf\['db_host'\] = getenv('CLOUDRON_MYSQL_HOST');/" \
        -i /app/data/local/config/database.inc.php
 
    cat << 'EOT' > /app/data/local/config/config.inc.php
<?php
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') $_SERVER['HTTPS']='on';

$conf['send_bcc_mail_webmaster'] = false;
$conf['mail_allow_html'] = true;

$conf['mail_sender_name'] = getenv('CLOUDRON_MAIL_FROM_DISPLAY_NAME') ?? 'Piwigo';
$conf['mail_sender_email'] = getenv('CLOUDRON_MAIL_FROM');
$conf['smtp_host'] = getenv('CLOUDRON_MAIL_SMTP_SERVER') . ':' . getenv('CLOUDRON_MAIL_SMTP_PORT');
$conf['smtp_user'] = getenv('CLOUDRON_MAIL_SMTP_USERNAME');
$conf['smtp_password'] = getenv('CLOUDRON_MAIL_SMTP_PASSWORD');
$conf['smtp_secure'] = null;
?>
EOT

    echo "=> Piwigo initialized"
}

if [[ ! -f /app/data/local/config/config.inc.php ]]; then
    echo "=> First run"
    ( setup ) &
else
    echo "=> Fixup permissions"
    chown -R www-data.www-data /app/data /run/piwigo
fi

echo "=> Run apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
