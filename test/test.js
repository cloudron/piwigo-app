#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const LOCAL_FILEPATH = path.resolve(__dirname, 'berlin.jpg');
    const ALBUME_NAME = 'vacationpics';

    var browser;
    var app;
    var adminUser = 'admin';
    var adminPassword = 'changeme';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function firstLogin() {
        await browser.get(`https://${app.fqdn}/identification.php`);

        await waitForElement(By.xpath('//form[@name="login_form"]//input[@name="username"]'));
        await browser.findElement(By.xpath('//form[@name="login_form"]//input[@name="username"]')).sendKeys(adminUser);
        await browser.findElement(By.xpath('//form[@name="login_form"]//input[@name="password"]')).sendKeys(adminPassword);
        await browser.findElement(By.xpath('//form[@name="login_form"]//input[@name="login"]')).click();

        await waitForElement(By.id('deactivate'));
        await browser.get(`https://${app.fqdn}/?no_photo_yet=deactivate`);

        await waitForElement(By.xpath('//a[contains(text(), "Logout")]'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/identification.php`);

        await waitForElement(By.xpath('//form[@name="login_form"]//input[@name="username"]'));
        await browser.findElement(By.xpath('//form[@name="login_form"]//input[@name="username"]')).sendKeys(adminUser);
        await browser.findElement(By.xpath('//form[@name="login_form"]//input[@name="password"]')).sendKeys(adminPassword);
        await browser.findElement(By.xpath('//form[@name="login_form"]//input[@name="login"]')).click();

        await waitForElement(By.xpath('//a[contains(text(), "Logout")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//a[contains(text(), "Logout")]'));
        await browser.findElement(By.xpath('//a[contains(text(), "Logout")]')).click();
        await waitForElement(By.xpath('//a[contains(text(), "Login")]'));
    }

    async function addAlbum() {
        await browser.get(`https://${app.fqdn}/admin.php?page=cat_list`);

        await waitForElement(By.xpath('//div[@class="addAlbumHead"]'));
        await browser.findElement(By.xpath('//div[@class="addAlbumHead"]')).click();

        await waitForElement(By.xpath('//input[@name="virtual_name"]'));
        await browser.findElement(By.xpath('//input[@name="virtual_name"]')).sendKeys(ALBUME_NAME);

        await waitForElement(By.xpath('//button[@name="submitAdd"]'));
        await browser.findElement(By.xpath('//button[@name="submitAdd"]')).click();

        await waitForElement(By.xpath(`//div[contains(text(), "${ALBUME_NAME}")]`));
    }

    async function albumExists() {
        await browser.get(`https://${app.fqdn}/admin.php?page=photos_add&album=1`);

        await waitForElement(By.xpath('//h1[text()="Upload Photos"]'));
    }

    async function uploadFile() {
        await browser.get(`https://${app.fqdn}/admin.php?page=photos_add&album=1`);

        await waitForElement(By.xpath('//h1[text()="Upload Photos"]'));
        await browser.findElement(By.xpath('//input[@type="file"]')).sendKeys(LOCAL_FILEPATH);

        await browser.sleep(1000);

        await waitForElement(By.id('startUpload'));
        await browser.findElement(By.id('startUpload')).click();

        await waitForElement(By.xpath('//li[text()="1 photos uploaded"]'));
    }

    async function fileExists() {
        await browser.get(`https://${app.fqdn}/picture.php?/1/category/1`);

        await waitForElement(By.id('theMainImage'));
        var img = browser.findElement(By.id('theMainImage'));

        await browser.sleep(5000); // give the image sometime to load

        await waitForElement(By.xpath('//img[@alt="berlin.jpg"]'));
    }

    async function upgrade() {
        await browser.get(`https://${app.fqdn}/upgrade.php`);

        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(adminUser);
        await browser.findElement(By.id('password')).sendKeys(adminPassword);
        await browser.findElement(By.xpath('//input[@type="submit"]')).click();

        await browser.sleep(5000);

        await browser.get('about:blank');
        await browser.get(`https://${app.fqdn}/upgrade.php`);
        await waitForElement(By.xpath('//*[text()="No upgrade required, the database structure is up to date"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can first login', firstLogin);
    it('can add album', addAlbum);
    it('album exists', albumExists);
    it('can upload file', uploadFile);
    it('can uploaded file exists', fileExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('album exists', albumExists);
    it('can uploaded file exists', fileExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('album exists', albumExists);
    it('can uploaded file exists', fileExists);
    it('can logout', logout);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);

        getAppInfo();
    });

    it('can login', login);
    it('album exists', albumExists);
    it('can uploaded file exists', fileExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id org.piwigo.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', firstLogin);
    it('can add album', addAlbum);
    it('album exists', albumExists);
    it('can upload file', uploadFile);
    it('can uploaded file exists', fileExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can login', login);
    it('album exists', albumExists);
    it('can uploaded file exists', fileExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
