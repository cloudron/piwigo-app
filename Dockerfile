FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# install exiftools and jpegtran
RUN apt update && apt-get install -y libimage-exiftool-perl libjpeg-progs &&  \
    rm -rf /var/cache/apt /var/lib/apt/lists

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/piwigo.conf /etc/apache2/sites-enabled/piwigo.conf
RUN a2disconf other-vhosts-access-log && a2dismod perl && a2enmod rewrite env remoteip
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/piwigo/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-releases depName=Piwigo/Piwigo versioning=semver
ARG PIWIGO_VERSION=15.4.0

# get piwigo
RUN wget -O piwigo-${PIWIGO_VERSION}.zip https://piwigo.org/download/dlcounter.php?code=${PIWIGO_VERSION} && \
    unzip piwigo-${PIWIGO_VERSION}.zip && \
    rm piwigo-${PIWIGO_VERSION}.zip

# move folders around
RUN mv /app/code/piwigo/_data /app/code/piwigo/_data_old && \
    mv /app/code/piwigo/plugins /app/code/piwigo/_plugins && \
    mv /app/code/piwigo/themes /app/code/piwigo/_themes && \
    mv /app/code/piwigo/language /app/code/piwigo/_language && \
    mv /app/code/piwigo/local /app/code/piwigo/_local && \
    mv /app/code/piwigo/* /app/code/ && \
    rm -R /app/code/piwigo/ /app/code/upload

# link to static directories
RUN ln -sf /app/data/plugins /app/code/plugins && \
    ln -sf /app/data/_data /app/code/_data && \
    ln -sf /app/data/language /app/code/language && \
    ln -sf /app/data/themes /app/code/themes && \
    ln -sf /app/data/local/ /app/code/local && \
    ln -sf /app/data/upload /app/code/upload && \
    rm -rf /app/code/galleries && ln -sf /app/data/galleries /app/code/galleries

RUN chown -R www-data.www-data /app/code

ADD start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
