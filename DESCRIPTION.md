Piwigo is open source photo gallery software for the web. Designed for organisations, teams and individuals.

### Why choose Piwigo?

* High Volume - Piwigo shines when it comes to classifying thousands or even hundreds of thousands of photos.
* Sustainable - Born in 2002, Piwigo has been supporting its users for more than 21 years. Always evolving!
* Open Source - Source code is available, editable, auditable and extendable thanks to plugins and themes.

### Features

* Albums Hierarchy
* Batch Management
* Themes & Plugins
* Permissions Control

